import { connect } from 'react-redux';
import { openModal } from '../actions/actions';
import Popular from '../components/Portal/Popular';

const mapDispatchToProps = (dispatch) => ({
  openModal: () => {
    dispatch(openModal());
  },
});

const mapStateToProps = (state) => ({
  popularTileOne: state.popularReducer.get('popularTileOne'),
  popularTileTwo: state.popularReducer.get('popularTileTwo'),
  popularTileThree: state.popularReducer.get('popularTileThree'),
  popularTileFour: state.popularReducer.get('popularTileFour'),
  popularTileFive: state.popularReducer.get('popularTileFive'),
  popularTileSix: state.popularReducer.get('popularTileSix'),
});

const PopularContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Popular);
export default PopularContainer;
