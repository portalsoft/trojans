import { connect } from 'react-redux';
import Partnerships from '../components/Portal/Partnerships';

const mapStateToProps = (state) => ({
  workWithTileOne: state.popularReducer.get('workWithTileOne'),
  workWithTileTwo: state.popularReducer.get('workWithTileTwo'),
  workWithTileThree: state.popularReducer.get('workWithTileThree'),
  workWithTileFour: state.popularReducer.get('workWithTileFour'),
  workWithTileFive: state.popularReducer.get('workWithTileFive'),
  workWithTileSix: state.popularReducer.get('workWithTileSix'),
});

const PartnershipsContainer = connect(
  mapStateToProps,
  null
)(Partnerships);
export default PartnershipsContainer;
