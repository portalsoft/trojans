import { connect } from 'react-redux';
import {
  toggleMenu,
  toggleSearch,
  recommendedLinkClick,
  categoriesLinkClick,
  partnershipsLinkClick,
} from '../actions/actions';
import Home from '../components/Portal/Home';

const mapDispatchToProps = (dispatch) => ({
  toggle: () => {
    dispatch(toggleMenu());
  },
  toggleSearch: () => {
    dispatch(toggleSearch());
  },
  recommendedLinkClick: () => {
    dispatch(recommendedLinkClick());
  },
  categoriesLinkClick: () => {
    dispatch(categoriesLinkClick());
  },
  partnershipsLinkClick: () => {
    dispatch(partnershipsLinkClick());
  },
});

const mapStateToProps = (state) => ({
  isMenuOpen: state.homeReducer.get('isMenuOpen'),
  searchClasses: state.homeReducer.get('searchClasses'),
  menuIconClasses: state.homeReducer.get('menuIconClasses'),
  menuClasses: state.homeReducer.get('menuClasses'),
  searchCompanies: state.homeReducer.get('searchCompanies'),
});

const HomeContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
export default HomeContainer;
