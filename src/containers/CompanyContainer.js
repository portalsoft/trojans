import { connect } from 'react-redux';
import Company from '../components/Modal/Company';
import { attemptLogin, claim, gTwoCrowdClick } from "../actions/actions";

const mapStateToProps = (state) => ({
  isLoggedIn: state.userReducer.get('isLoggedIn'),
  message: state.userReducer.get('message'),
});

const mapDispatchToProps = (dispatch) => ({
  attemptLogin: (code) => {
    dispatch(attemptLogin(code));
  },
  claim: () => {
    dispatch(claim());
  },
  gTwoCrowdClick: () => {
    dispatch(gTwoCrowdClick());
  },
});

const CompanyContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Company);
export default CompanyContainer;
