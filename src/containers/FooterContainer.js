import { connect } from 'react-redux';
import { openModal, blogClick, contactUsClick, toggleSearch } from '../actions/actions';
import Footer from '../components/Portal/Footer';

const mapDispatchToProps = (dispatch) => ({
  openModal: () => {
    dispatch(openModal());
  },
  contactUsClick: () => {
    dispatch(contactUsClick());
  },
  toggleSearch: () => {
    dispatch(toggleSearch());
  },
  blogClick: () => {
    dispatch(blogClick());
  },
});

const FooterContainer = connect(
  null,
  mapDispatchToProps
)(Footer);
export default FooterContainer;
