import { connect } from 'react-redux';
import { openModal } from '../actions/actions';
import Categories from '../components/Portal/Categories';

const mapDispatchToProps = (dispatch) => ({
  openModal: () => {
    dispatch(openModal());
  },
});

const CategoriesContainer = connect(
  null,
  mapDispatchToProps
)(Categories);
export default CategoriesContainer;
