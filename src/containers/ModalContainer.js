import { connect } from 'react-redux';
import { closeModal } from '../actions/actions';
import Modal from '../components/Modal';

const mapDispatchToProps = (dispatch) => ({
  closeModal: () => {
    dispatch(closeModal());
  },
});

const mapStateToProps = (state) => ({
  modalClasses: state.buttonReducer.get('modalClasses'),
  modalOverlayClasses: state.buttonReducer.get('modalOverlayClasses'),
  backButtonClasses: state.buttonReducer.get('backButtonClasses'),
  modalParams: state.buttonReducer.get('modalParams'),
});

const ModalContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Modal);
export default ModalContainer;
