import { connect } from 'react-redux';
import { checkLogin } from '../actions/actions';
import Login from '../components/Login';

const mapDispatchToProps = (dispatch) => ({
  checkLogin: () => {
    dispatch(checkLogin());
  },
});

const LoginContainer = connect(
  null,
  mapDispatchToProps
)(Login);
export default LoginContainer;
