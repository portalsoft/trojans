import React from 'react';
import ReactDOM from 'react-dom';

class ScrollTop extends React.Component {
  componentDidMount() {
    if (Math.max(document.documentElement.clientWidth, window.innerWidth || 0) <= 670) {
      ReactDOM.findDOMNode(this).scrollIntoView();
    }
  }
  render() {
    return(
      <div className="scroll-top">
      </div>
    )
  }
}
export default ScrollTop;
