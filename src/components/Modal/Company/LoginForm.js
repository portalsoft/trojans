import React from 'react';

class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inputValue: '',
    };
  }
  promoHandler() {
    if ( (this.props.seedPromo != undefined) && window.localStorage['seedStar'] === 'true') {
      return `Promo Code: ${this.props.seedPromo}`;
    } else if (this.props.promo != undefined) {
      return `Promo Code: ${this.props.promo}`;
    } else {
      return "";
    }
  }
  updateInputValue(evt) {
    this.setState({
      inputValue: evt.target.value,
    });
  }
  checkEnter(evt) {
    if (evt.key === 'Enter') {
      this.attemptLogin();
    }
  }
  attemptLogin() {
    this.props.attemptLogin(this.state.inputValue)
  }
  render() {
    if (this.props.isLoggedIn) {
      return(
        <div className="profile__button-wrapper">
          <div className="profile__verification__promo">
            {this.promoHandler()}
          </div>
          <a
            onClick={() => this.props.claim()}
            href={this.props.url}
            target="_blank"
            className="profile__button"
          >
            <div className="profile__button__button-text">
              Claim Now!
            </div>
          </a>
        </div>
      )
    }
    return(
      <div className="profile__button-wrapper">
        <input
          className="profile__button-wrapper__form-field"
          type="text"
          value={this.state.inputValue}
          onChange={this.updateInputValue.bind(this)}
          onKeyPress={this.checkEnter.bind(this)}
          placeholder="Enter your code..."
        />
        <div
          className="profile__button"
          onClick={this.attemptLogin.bind(this)}
        >
          <div className="profile__button__button-text">
            Enter
          </div>
        </div>
      </div>
    )
  }
}
export default LoginForm;
