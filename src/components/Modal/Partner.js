import React from 'react';

const Partner = () => (
  <div className="modal-content">
    <div className="category-content">
      <div className='category-header'>
        <h1 className="category-header-text">
          Communities
        </h1>
      </div>
      <div className="terms-wrapper">
        <div className="terms-section">
          At PortalSoft, we are in the business of supporting entrepreneurial communities. Working side-by-side with VC’s, accelerators, and co-working spaces, we ensure their members have the right growth tools at enterprise prices. We personally source and aggregate these discounts based on the needs of our community.
        </div>
        <div className="terms-section">
          Our PortalSoft platform is completely free to use with no software integration necessary. Using our shared buying power of our 55,000 strong community, we leverage our size to secure enterprise pricing on best-in-class products.
        </div>
        <div className="terms-section">
          With over 40 different offers in Sales, Marketing, Travel, HR, Productivity, Legal, and Finance, we cover all the tools a growing startup needs. Invest in the success of your community while driving retention by adding the free PortalSoft platform today.
        </div>
        <div className="terms-section">
          Please email <a
              href="mailto:michael@portalsoft.io?Subject=General%20Inquiry"
            >Michael@portalsoft.io</a> to find out more.
        </div>
      </div>
    </div>
  </div>
);
export default Partner;
