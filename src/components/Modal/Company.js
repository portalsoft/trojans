import React from 'react';
import PropTypes from 'prop-types';
import LoginForm from './Company/LoginForm';
import ScrollTop from './Company/ScrollTop';

const logoStyles = (backgroundImage) => ({
  flex: '.5',
  backgroundImage: `url(${backgroundImage})`,
  backgroundPosition: 'center',
  backgroundSize: '75% auto',
  backgroundRepeat: 'no-repeat',
  backgroundColor: '#ffffff',
  width: '100%',
  height: '100%',
  flex: '.5 1 0%',
  WebkitFlex: '.5 1 0%',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'flex-end',
});

const screenshotStyles = (currentScreenshotImage) => ({
  backgroundImage: `url(${currentScreenshotImage})`,
  backgroundPosition: 'center',
  backgroundSize: '700px 394px',
  backgroundRepeat: 'no-repeat',
  width: '700px',
  minHeight: '394px',
  border: '1px solid #696969'
});

const urlHelper = (review)=> {
  let url;
  if (review == undefined) {
    url = '';
  } else {
    url = review.url;
  }
  return url;
}

const starHelper = (review, number) => {
  let rating;
  if (review == undefined) {
    rating = 40;
  } else {
    rating = review.rating
  }
  let comparisonNum = number * 10
  if (rating >= comparisonNum) {
    return "star star--filled";
  } else if (comparisonNum - rating < 10) {
    return "star star--half"
  } else {
    return "star"
  }
};

const screenshotHelper = (screenshots) => {
  if (screenshots == undefined) {
    return [];
  } else if (screenshots.length !== 4) {
    return screenshots.slice(1).push('')
  } else {
    return screenshots.slice(1)
  }
}

const Company = ({ childParams, isLoggedIn, message, attemptLogin, claim, gTwoCrowdClick }) => (
  <div className="company">
    <div className="company__description">
      <h1 className="company__description__header">
        {childParams.name}
      </h1>
      <div className="company__description__tagline">
        {childParams.tagline}
      </div>
      <div className="company__description__write-up-wrapper">
        <div className="company__description__write-up-wrapper__write-up">
          {childParams.writeUp}
        </div>
      </div>
      <div className='company__description__screenshot-wrapper'>
        <div className='company__description__screenshot-wrapper__screenshots--large'>
        {
          screenshotHelper(childParams.screenshots).map ((screenshot, index) => (
              <a
                key={index}
                href={childParams.infoLink}
                target="_blank"
              >
              <div className='company-screenshot' style={screenshotStyles(screenshot)} />
              </a>
            )
          )
        }
        </div>
      </div>
    </div>
    <div className="profile">
      <ScrollTop />
      <div className='profile__box'>
        <div className="profile__logo" style={logoStyles(childParams.imgUrl)} >
          <div className="profile__box__discount">
            {childParams.discount}
          </div>
        </div>
        <div className="profile__box__info">
          <div className="profile__box__info__spacing" />
          <div className="profile__box__info__line">
            {childParams.subCategory}
          </div>
          <div className="profile__box__info__line">
            <div className="stars">
              <span className={starHelper(childParams.review, 1)}></span>
              <span className={starHelper(childParams.review, 2)}></span>
              <span className={starHelper(childParams.review, 3)}></span>
              <span className={starHelper(childParams.review, 4)}></span>
              <span className={starHelper(childParams.review, 5)}></span>
              <span className="star star--spacer"></span>
              <a
               href={urlHelper(childParams.review)}
               target="_blank"
               className="footer-text-section"
               onClick={() => gTwoCrowdClick()}
              >
                See Reviews
              </a>
            </div>
          </div>
          <div className="profile__box__info__line">
          </div>
          <div className="profile__box__info__line">
          </div>
          <div className="profile__box__info__line">
          </div>
        </div>
      </div>
      <div className="profile__spacing" />
      <div className="profile__message">
        {message}
      </div>
      <div className="profile__spacing" />
      <LoginForm
        url={childParams.url}
        isLoggedIn={isLoggedIn}
        message={message}
        attemptLogin={attemptLogin}
        promo={childParams.promo}
        seedPromo={childParams.seedPromo}
        claim={claim}
      />
      <div className="profile__restrictions">
        <span className="bold">
          {childParams.restrictions}
        </span>
      </div>
      <div className="profile__spacing" />
    </div>
  </div>
);
Company.propTypes = {
  childParams: PropTypes.object,
  isLoggedIn: PropTypes.bool,
  message: PropTypes.string,
  attemptLogin: PropTypes.func,
  claim: PropTypes.func,
  gTwoCrowdClick: PropTypes.func,
};
export default Company;
