import React from 'react';

const Terms = () => (
  <div className="modal-content">
    <div className="about">
      <h1 className="about__header">
        TERMS AND CONDITIONS
      </h1>
      <div className="about__sub-header">
        Introduction
      </div>
      <div className="about__section">
        These terms and conditions govern your use of this website; by using this website, you accept these terms and conditions in full. If you disagree with these terms and conditions or any part of these terms and conditions, you must not use this website.
      </div>
      <div className="about__section">
        This website uses cookies.  By using this website and agreeing to these terms and conditions, you consent to our PortalSoft's use of cookies in accordance with the terms of PortalSoft's privacy policy.
      </div>
      <div className="about__sub-header">
        License to use website
      </div>
      <div className="about__section">
        Unless otherwise stated, PortalSoft and/or its licensors own the intellectual property rights in the website and material on the website.  Subject to the license below, all these intellectual property rights are reserved.
      </div>
      <div className="about__section">
        You may view, download for caching purposes only, and print pages from the website for your own personal use, subject to the restrictions set out below and elsewhere in these terms and conditions.
      </div>
      <div className="about__section">
        You must not:
        <div className="about__bullets">
          • republish material from this website (including republication on another website);
        </div>
        <div className="about__bullets">
          • sell, rent or sub-license material from the website;
        </div>
        <div className="about__bullets">
          • show any material from the website in public;
        </div>
        <div className="about__bullets">
          • reproduce, duplicate, copy or otherwise exploit material on this website for a commercial purpose;
        </div>
        <div className="about__bullets">
          • edit or otherwise modify any material on the website;
        </div>
        <div className="about__bullets">
          • redistribute material from this website except for content specifically and expressly made available for redistribution
        </div>
      </div>
      <div className="about__sub-header">
        Acceptable use
      </div>
      <div className="about__section">
        You must not use this website in any way that causes, or may cause, damage to the website or impairment of the availability or accessibility of the website; or in any way which is unlawful, illegal, fraudulent or harmful, or in connection with any unlawful, illegal, fraudulent or harmful purpose or activity.
      </div>
      <div className="about__section">
        You must not use this website to copy, store, host, transmit, send, use, publish or distribute any material which consists of (or is linked to) any spyware, computer virus, Trojan horse, worm, keystroke logger, rootkit or other malicious computer software.
      </div>
      <div className="about__section">
        You must not conduct any systematic or automated data collection activities (including without limitation scraping, data mining, data extraction and data harvesting) on or in relation to this website without PortalSoft express written consent.
      </div>
      <div className="about__section">
        You must not use this website to transmit or send unsolicited commercial communications.
      </div>
      <div className="about__section">
        You must not use this website for any purposes related to marketing without PortalSoft express written consent.
      </div>
      <div className="about__sub-header">
        Restricted access
      </div>
      <div className="about__section">
        Access to certain areas of this website is restricted. PortalSoft reserves the right to restrict access to [other] areas of this website, or indeed this entire website, at PortalSoft discretion.
      </div>
      <div className="about__section">
        If PortalSoft provides you with a user ID and password to enable you to access restricted areas of this website or other content or services, you must ensure that the user ID and password are kept confidential.
      </div>
      <div className="about__section">
        PortalSoft may disable your user ID and password in PortalSoft sole discretion without notice or explanation.
      </div>
      <div className="about__sub-header">
        User content
      </div>
      <div className="about__section">
        In these terms and conditions, “your user content” means material (including without limitation text, images, audio material, video material and audio-visual material) that you submit to this website, for whatever purpose.
      </div>
      <div className="about__section">
        You grant to PortalSoft a worldwide, irrevocable, non-exclusive, royalty-free license to use, reproduce, adapt, publish, translate and distribute your user content in any existing or future media.  You also grant to PortalSoft the right to sub-license these rights, and the right to bring an action for infringement of these rights.
      </div>
      <div className="about__section">
        Your user content must not be illegal or unlawful, must not infringe any third party's legal rights, and must not be capable of giving rise to legal action whether against you or PortalSoft or a third party (in each case under any applicable law).
      </div>
      <div className="about__section">
        You must not submit any user content to the website that is or has ever been the subject of any threatened or actual legal proceedings or other similar complaint.
      </div>
      <div className="about__section">
        PortalSoft reserves the right to edit or remove any material submitted to this website, or stored on PortalSoft servers, or hosted or published upon this website.
      </div>
      <div className="about__sub-header">
        No warranties
      </div>
      <div className="about__section">
        This website is provided “as is” without any representations or warranties, express or implied.  PortalSoft makes no representations or warranties in relation to this website or the information and materials provided on this website.
      </div>
      <div className="about__section">
        Without prejudice to the generality of the foregoing paragraph, PortalSoft does not warrant that:
        <div className="about__bullets">
          • this website will be constantly available, or available at all; or
        </div>
        <div className="about__bullets">
          • the information on this website is complete, true, accurate or non-misleading.
        </div>
      </div>
      <div className="about__section">
        Nothing on this website constitutes, or is meant to constitute, advice of any kind.
      </div>
      <div className="about__sub-header">
        Limitations of liability
      </div>
      <div className="about__section">
        PortalSoft will not be liable to you (whether under the law of contact, the law of torts or otherwise) in relation to the contents of, or use of, or otherwise in connection with, this website:
        <div className="about__bullets">
          • to the extent that the website is provided free-of-charge, for any direct loss;
        </div>
        <div className="about__bullets">
          • for any indirect, special or consequential loss; or
        </div>
        <div className="about__bullets">
          • for any business losses, loss of revenue, income, profits or anticipated savings, loss of contracts or business relationships, loss of reputation or goodwill, or loss or corruption of information or data.
        </div>
      </div>
      <div className="about__section">
        These limitations of liability apply even if PortalSoft has been expressly advised of the potential loss.
      </div>
      <div className="about__sub-header">
        Exceptions
      </div>
      <div className="about__section">
        Nothing in this website disclaimer will exclude or limit any warranty implied by law that it would be unlawful to exclude or limit; and nothing in this website disclaimer will exclude or limit PortalSoft liability in respect of any:
        <div className="about__bullets">
          • death or personal injury caused by PortalSoft negligence;
        </div>
        <div className="about__bullets">
          • fraud or fraudulent misrepresentation on the part of PortalSoft; or
        </div>
        <div className="about__bullets">
          • matter which it would be illegal or unlawful for PortalSoft to exclude or limit, or to attempt or purport to exclude or limit, its liability.
        </div>
      </div>
      <div className="about__sub-header">
        Reasonableness
      </div>
      <div className="about__section">
        By using this website, you agree that the exclusions and limitations of liability set out in this website disclaimer are reasonable.
      </div>
      <div className="about__section">
        If you do not think they are reasonable, you must not use this website.
      </div>
      <div className="about__sub-header">
        Other parties
      </div>
      <div className="about__section">
        You accept that, as a limited liability entity, PortalSoft has an interest in limiting the personal liability of its officers and employees.  You agree that you will not bring any claim personally against PortalSoft officers or employees in respect of any losses you suffer in connection with the website.
      </div>
      <div className="about__section">
        Without prejudice to the foregoing paragraph, you agree that the limitations of warranties and liability set out in this website disclaimer will protect PortalSoft officers, employees, agents, subsidiaries, successors, assigns and sub-contractors as well as PortalSoft.
      </div>
      <div className="about__sub-header">
        Unenforceable provisions
      </div>
      <div className="about__section">
        If any provision of this website disclaimer is, or is found to be, unenforceable under applicable law, that will not affect the enforceability of the other provisions of this website disclaimer.
      </div>
      <div className="about__sub-header">
        Indemnity
      </div>
      <div className="about__section">
        You hereby indemnify PortalSoft and undertake to keep PortalSoft indemnified against any losses, damages, costs, liabilities and expenses (including without limitation legal expenses and any amounts paid by PortalSoft to a third party in settlement of a claim or dispute on the advice of PortalSoft legal advisers) incurred or suffered by PortalSoft arising out of any breach by you of any provision of these terms and conditions[, or arising out of any claim that you have breached any provision of these terms and conditions.
      </div>
      <div className="about__sub-header">
        Breaches of these terms and conditions
      </div>
      <div className="about__section">
        Without prejudice to PortalSoft other rights under these terms and conditions, if you breach these terms and conditions in any way, PortalSoft may take such action as PortalSoft deems appropriate to deal with the breach, including suspending your access to the website, prohibiting you from accessing the website, blocking computers using your IP address from accessing the website, contacting your internet service provider to request that they block your access to the website and/or bringing court proceedings against you.
      </div>
      <div className="about__sub-header">
        Variation
      </div>
      <div className="about__section">
        PortalSoft may revise these terms and conditions from time-to-time.  Revised terms and conditions will apply to the use of this website from the date of the publication of the revised terms and conditions on this website.  Please check this page regularly to ensure you are familiar with the current version.
      </div>
      <div className="about__sub-header">
        Assignment
      </div>
      <div className="about__section">
        PortalSoft may transfer, sub-contract or otherwise deal with PortalSoft rights and/or obligations under these terms and conditions without notifying you or obtaining your consent.
      </div>
      <div className="about__section">
        You may not transfer, sub-contract or otherwise deal with your rights and/or obligations under these terms and conditions.
      </div>
      <div className="about__sub-header">
        Severability
      </div>
      <div className="about__section">
        If a provision of these terms and conditions is determined by any court or other competent authority to be unlawful and/or unenforceable, the other provisions will continue in effect.  If any unlawful and/or unenforceable provision would be lawful or enforceable if part of it were deleted, that part will be deemed to be deleted, and the rest of the provision will continue in effect.
      </div>
      <div className="about__sub-header">
        Entire agreement
      </div>
      <div className="about__section">
        These terms and conditions constitute the entire agreement between you and PortalSoft in relation to your use of this website, and supersede all previous agreements in respect of your use of this website.
      </div>
      <div className="about__sub-header">
        Law and jurisdiction
      </div>
      <div className="about__section">
        These terms and conditions will be governed by and construed and any disputes relating to these terms and conditions will be subject to the exclusive jurisdiction of the courts.
      </div>
      <div className="about__sub-header">
        PortalSoft details
      </div>
      <div className="about__section about__section--last">
        You can contact PortalSoft by email to michael@portalsoft.io
      </div>
    </div>
  </div>
);
export default Terms;
