import React from 'react';

const About = () => (
  <div className="modal-content">
    <div className="about__wrapper">
      <h1 className="about__header">
        About
      </h1>
      <div className="about__section">
        When starting a business, every penny counts. We built PortalSoft with this concept in mind. Our community-first platform lets you search and discover the right growth tools at heavily discounted rates. Our vision is to support entrepreneurs in the most efficient and cost-effective way. This is why we offer our platform completely free to all entrepreneurial communities and their members.
      </div>
      <div className="about__section">
        Our 55,000+ strong community allows us to negotiate enterprise level pricing from the top B2B companies. Over 40 leading vendors have chosen to partner with PortalSoft and more are announced every month. These trusted companies support our goals of helping startups succeed and work with us to ensure the highest level of customer support for every member. We are excited to make this platform available to our members, and we hope the best for you and your business.
      </div>
      <div className="about__section">
        We love feedback! If there is a community you want to nominate or products you’d like to see on the list please reach out and let us know.
      </div>
    </div>
  </div>
);
export default About;
