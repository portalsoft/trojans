import React from 'react';

class Empty extends React.Component {
  componentWillMount() {
    this.props.closeModal();
  }
  render() {
    return (<div></div>);
  }
};
export default Empty;
