import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';

const imageStyles = (backgroundImage) => {
  backgroundImage = ("https://portal-soft.imgix.net/" + backgroundImage.match(/https:\/\/firebasestorage.googleapis.com\/v0\/b\/portalsoft-fec45.appspot.com\/o\/[^?]+/g)[0].slice(75))
  return {
    backgroundImage: `url(${backgroundImage})`,
    backgroundPosition: 'center',
    backgroundSize: '75% auto',
    backgroundRepeat: 'no-repeat',
    width: '100%',
    flex: '4 1 0%',
    WebkitFlex: '4 1 0%',
  }
};

const Category = ({ childParams }) => (
  <div className="modal-content">
    <div className="page">
      <div className='page__header page__header--modal'>
        <h1 className="page__header__text">
          {childParams.category.header}
        </h1>
      </div>
      <div className="page__tiles page__tiles--modal">
        {
        childParams.category.content.map((company, index) => (
          <Link
            className="tile tile--company tile--modal"
            key={index}
            to={`/categories/${childParams.category.id}/companies/${company.id}`}
          >
            <div
              style={imageStyles(company.imgUrl)}
            >
            </div>
            <div className="tile__name">
              {company.name}
            </div>
            <div className="tile__discount">
              {company.discount}
            </div>
            <div className="tile__tagline" >
              {company.tagline}
            </div>
          </Link>
        ))
        }
      </div>
      <div className="category-content-spacer" />
    </div>
  </div>
);
Category.propTypes = {
  childParams: PropTypes.object,
};
export default Category;
