import React from 'react';

const Vendor = () => (
  <div className="modal-content">
    <div className="category-content">
      <div className='category-header'>
        <h1 className="category-header-text">
          Vendors
        </h1>
      </div>
      <div className="terms-wrapper">
        <div className="terms-section">
          Have a great tool for SME’s? Looking for a new marketing channel?
        </div>
        <div className="terms-section">
          Become an vendor on the PortalSoft platform and access over 55,000 entrepreneurs, absolutely free. We are the exclusive tech benefits provider for entrepreneurial communities including VC’s, accelerators, and co-working spaces across the globe.
        </div>
        <div className="terms-section">
          Our trusted platform lets startups discover and learn about your product without the traditional marketing noise. PortalSoft also supplies sales, marketing, and customer support for your product to ensure leads don’t slip through the cracks. Boost your visibility and attract new customers by joining the PortalSoft platform today.
        </div>
        <div className="terms-section">
          Please email <a
              href="mailto:michael@portalsoft.io?Subject=General%20Inquiry"
            >Michael@portalsoft.io</a> to find out more.
        </div>
      </div>
    </div>
  </div>
);
export default Vendor;
