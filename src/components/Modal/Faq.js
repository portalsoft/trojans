import React from 'react';

const Faq = () => (
  <div className="modal-content">
    <div className="about__wrapper">
      <h1 className="about__header">
        FAQ
      </h1>
      <div className="about__sub-header">
        Why use the PortalSoft marketplace?
      </div>
      <div className="about__section">
        Redeeming offers through the marketplace enables you to access discounts exclusive to PortalSoft members of 15% or more.
      </div>
      <div className="about__sub-header">
        How do I change or cancel a service subscription?
      </div>
      <div className="about__section">
        Contact the company directly about any changes or cancellation to your service. The companies terms of services apply to anyone who signs up through PortalSoft.
      </div>
      <div className="about__sub-header">
        What happens to my service subscriptions if I leave PortalSoft?
      </div>
      <div className="about__section">
        Leaving the members community will not affect your current subscription for the remaining period. Any extensions will be priced at normal cost.
      </div>
      <div className="about__sub-header">
        Still have some questions?
      </div>
      <div className="about__section">
        Feel free to send them our way at michael@portalsoft.io.
      </div>
    </div>
  </div>
);
export default Faq;
