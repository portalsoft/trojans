import React from 'react';
import PropTypes from 'prop-types';
import HomeContainer from '../containers/HomeContainer';
import PopularContainer from '../containers/PopularContainer';
import CategoriesContainer from '../containers/CategoriesContainer';
import PartnershipsContainer from '../containers/PartnershipsContainer';
import FooterContainer from '../containers/FooterContainer';
import ModalContainer from '../containers/ModalContainer';
import ScrollPageTop from './Helpers/ScrollPageTop';

const Portal = ({ children }) => (
  <div>
    <ScrollPageTop />
    <h1 className="seo-header">Portal Soft</h1>
    <div id="flex-wrapper" className="app">
      <HomeContainer />
      <PopularContainer />
      <CategoriesContainer />
      <PartnershipsContainer />
      <FooterContainer />
    </div>
    <ModalContainer children={children} />
  </div>
);
Portal.propTypes = {
  children: PropTypes.object,
};
export default Portal;
