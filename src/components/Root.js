import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import { Route, Router, browserHistory, IndexRoute } from 'react-router';
import { createStore, applyMiddleware } from 'redux';
import { syncHistoryWithStore } from 'react-router-redux';
import dbMiddleware from '../middleware/dbMiddleware';
import rootReducer from '../reducers/rootReducer';
import Portal from './Portal';
import CompanyContainer from '../containers/CompanyContainer';
import Category from './Modal/Category';
import Faq from './Modal/Faq';
import About from './Modal/About';
import Terms from './Modal/Terms';
import Empty from './Modal/Empty';
import Vendor from './Modal/Vendor';
import Partner from './Modal/Partner';
import Splash from './Splash';

const store = createStore(
  rootReducer,
  applyMiddleware(dbMiddleware)
);

const history = syncHistoryWithStore(browserHistory, store);

const redirectUser = (nextState, replace) => {
  replace({
    pathname: '/',
  });
};

const logoutUser = (nextState, replace) => {
  replace({
    pathname: '/login',
  });
};

const Root = () => (
  <Provider store={store}>
    <Router key="0" history={history} >
      <Route
        key="1"
        path="/partnerships"
        component={Splash}
      />
      <Route
        key="2"
        path="/logout"
        onEnter={logoutUser}
      />
      <Route
        key="3"
        path="/"
        component={Portal}
      >
        <IndexRoute
          key="4"
          component={Empty}
        />
        <Route
          key="5"
          path="companies/:company"
          component={CompanyContainer}
        />
        <Route
          key="6"
          path="categories/:category"
          component={Category}
        />
        <Route
          key="7"
          path="categories/(:category)/companies/:company"
          component={CompanyContainer}
        />
        <Route
          key="8"
          path="faq"
          component={Faq}
        />
        <Route
          key="9"
          path="about"
          component={About}
        />
        <Route
          key="10"
          path="terms"
          component={Terms}
        />
        <Route
          key="11"
          path="vendor"
          component={Vendor}
        />
        <Route
          key="12"
          path="partner"
          component={Partner}
        />
        <Route
          key="13"
          path="partner/companies"
          component={Category}
        />
        <Route
          key="14"
          path="partner/companies/:company"
          component={CompanyContainer}
        />
        <Route
          key="15"
          path="companies"
          component={Category}
        />
        <Route
          key="16"
          path='*'
          onEnter={redirectUser}
        />
      </Route>
    </Router>
  </Provider>
);
Root.propTypes = {
  store: PropTypes.object,
};
export default Root;
