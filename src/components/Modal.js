import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';

const Modal = ({ children, modalClasses, backButtonClasses, modalOverlayClasses, modalParams, closeModal }) => (
  <div>
    <div className={modalClasses}>
      <Link
        to={modalParams.backPath}
        className={backButtonClasses}
      >
        {modalParams.backArrow}
      </Link>
      {React.cloneElement(children, {
        key: Math.floor(Math.random() * 100),
        childParams: modalParams,
        closeModal,
      })}
    </div>
    <Link
      to="/"
      className={modalOverlayClasses}
    />
  </div>
);
Modal.propTypes = {
  children: PropTypes.object,
  modalClasses: PropTypes.string,
  modalOverlayClasses: PropTypes.string,
  backButtonClasses: PropTypes.string,
  modalParams: PropTypes.object,
  closeModal: PropTypes.func,
};
export default Modal;
