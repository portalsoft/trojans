import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';

const Categories = ({ openModal }) => (
  <div className="page page--blue" id="categories">
    <div className="page__header page__header--white">
      <div className='page__header__text'>
        Categories
      </div>
    </div>
    <div className="page__tiles">
      <Link
        className="tile tile--category"
        to="categories/1"
      >
        <div className="icon-container">
          <svg className="icon">
            <use xlinkHref="#icon-Productivity"></use>
          </svg>
        </div>
        <div className="tile__cat-name">
          Productivity
        </div>
      </Link>
      <Link
        className="tile tile--category"
        to="categories/0"
      >
        <div className="icon-container">
          <svg className="icon">
            <use xlinkHref="#icon-Finance"></use>
          </svg>
        </div>
        <div className="tile__cat-name">
          Finance & Legal
        </div>
      </Link>
      <Link
        className="tile tile--category"
        to="categories/2"
      >
        <div className="icon-container">
          <svg className="icon">
            <use xlinkHref="#icon-Sales"></use>
          </svg>
        </div>
        <div className="tile__cat-name">
          Sales
        </div>
      </Link>
      <Link
        className="tile tile--category"
        to="categories/3"
      >
        <div className="icon-container">
          <svg className="icon">
            <use xlinkHref="#icon-Marketing"></use>
          </svg>
        </div>
        <div className="tile__cat-name">
          Marketing
        </div>
      </Link>
      <Link
        className="tile tile--category"
        to="categories/4"
      >
        <div className="icon-container">
          <svg className="icon">
            <use xlinkHref="#icon-HR"></use>
          </svg>
        </div>
        <div className="tile__cat-name">
          HR & Benefits
        </div>
      </Link>
      <Link
        className="tile tile--category"
        to="categories/5"
      >
        <div className="icon-container">
          <svg className="icon">
            <use xlinkHref="#icon-Travel"></use>
          </svg>
        </div>
        <div className="tile__cat-name">
          Travel & Free
        </div>
      </Link>
    </div>
  </div>
);
Categories.propTypes = {
  openModal: PropTypes.func,
};
export default Categories;
