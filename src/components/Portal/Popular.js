import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';

const imageStyles = (backgroundImage) => ({
  backgroundImage: `url(${backgroundImage})`,
  backgroundPosition: 'center',
  backgroundSize: '75% auto',
  backgroundRepeat: 'no-repeat',
  width: '100%',
  flex: '3 1 0%',
  WebkitFlex: '3 1 0%',
});


const Popular = ({ openModal, popularTileOne, popularTileTwo, popularTileThree, popularTileFour, popularTileFive, popularTileSix }) => (
  <div className="page page--gray" id="recommended">
    <div className="page__header">
      <div className="page__header__text">
        Most Popular
      </div>
    </div>
    <div className="page__tiles">
      <Link
        className="tile tile--company"
        to={`companies/${popularTileOne.id}`}
      >
        <div
          style={imageStyles(popularTileOne.imgUrl)}
        >
        </div>
        <div className="tile__name">
          {popularTileOne.name}
        </div>
        <div className="tile__discount">
          {popularTileOne.discount}
        </div>
        <div className="tile__tagline" >
          {popularTileOne.tagline}
        </div>
      </Link>
      <Link
        className="tile tile--company"
        to={`companies/${popularTileTwo.id}`}
      >
        <div
          style={imageStyles(popularTileTwo.imgUrl)}
        >
        </div>
        <div className="tile__name">
          {popularTileTwo.name}
        </div>
        <div className="tile__discount">
          {popularTileTwo.discount}
        </div>
        <div className="tile__tagline" >
          {popularTileTwo.tagline}
        </div>
      </Link>
      <Link
        className="tile tile--company"
        to={`companies/${popularTileThree.id}`}
      >
        <div
          style={imageStyles(popularTileThree.imgUrl)}
        >
        </div>
        <div className="tile__name">
          {popularTileThree.name}
        </div>
        <div className="tile__discount">
          {popularTileThree.discount}
        </div>
        <div className="tile__tagline" >
          {popularTileThree.tagline}
        </div>
      </Link>
      <Link
        className="tile tile--company"
        to={`companies/${popularTileFour.id}`}
      >
        <div
          style={imageStyles(popularTileFour.imgUrl)}
        >
        </div>
        <div className="tile__name">
          {popularTileFour.name}
        </div>
        <div className="tile__discount">
          {popularTileFour.discount}
        </div>
        <div className="tile__tagline" >
          {popularTileFour.tagline}
        </div>
      </Link>
      <Link
        className="tile tile--company"
        to={`companies/${popularTileFive.id}`}
      >
        <div
          style={imageStyles(popularTileFive.imgUrl)}
        >
        </div>
        <div className="tile__name">
          {popularTileFive.name}
        </div>
        <div className="tile__discount">
          {popularTileFive.discount}
        </div>
        <div className="tile__tagline" >
          {popularTileFive.tagline}
        </div>
      </Link>
      <Link
        className="tile tile--company"
        to={`companies/${popularTileSix.id}`}
      >
        <div
          style={imageStyles(popularTileSix.imgUrl)}
        >
        </div>
        <div className="tile__name">
          {popularTileSix.name}
        </div>
        <div className="tile__discount">
          {popularTileSix.discount}
        </div>
        <div className="tile__tagline" >
          {popularTileSix.tagline}
        </div>
      </Link>
    </div>
  </div>
);
Popular.propTypes = {
  openModal: PropTypes.func,
};
export default Popular;
