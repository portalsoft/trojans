import React from 'react';
import { Link } from 'react-router';

const imageStyles = (backgroundImage) => {
  backgroundImage = ("https://portal-soft.imgix.net/" + backgroundImage.match(/https:\/\/firebasestorage.googleapis.com\/v0\/b\/portalsoft-fec45.appspot.com\/o\/[^?]+/g)[0].slice(75))
  return {
    backgroundImage: `url(${backgroundImage})`,
    backgroundPosition: 'center',
    backgroundSize: '80% auto',
    backgroundRepeat: 'no-repeat',
    width: '100%',
    flex: '2 1 0%',
    WebkitFlex: '2 1 0%',
  }
}

class Search extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inputValue: '',
      filteredCompanies: [],
    };
  }
  componentWillReceiveProps() {
    this.setState({
      filteredCompanies: this.props.searchCompanies
    })
  }
  updateInputValue(evt) {
    this.setState({
      inputValue: evt.target.value,
      filteredCompanies: this.props.searchCompanies.filter((company)=> {
        return company.name.toLowerCase().includes(evt.target.value.toLowerCase());
      }),
    });
  }
  render() {
    const companiesHelper = (filteredCompanies) => {
      if (filteredCompanies) {
        return filteredCompanies;
      } else {
        return [];
      }
    };
    return(
      <div className={this.props.searchClasses} >
        <div
          className="search-box__exit"
          onClick={() => this.props.toggleSearch()}
        >
          X
        </div>
        <div className="search-box__form">
          <input
            className="search-box__form__field"
            type="text"
            value={this.state.inputValue}
            onChange={this.updateInputValue.bind(this)}
            placeholder="Search all companies..."
          />
        </div>
        <div className="search-box__tiles">
          {
          companiesHelper(this.state.filteredCompanies).map((company, index) => (
            <Link
              className="tile tile--company"
              key={index}
              to={`/companies/${company.id}`}
            >
              <div
                style={imageStyles(company.imgUrl)}
              >
              </div>
              <div className="tile__name">
                {company.name}
              </div>
              <div className="tile__discount">
                {company.discount}
              </div>
              <div className="tile__tagline" >
                {company.tagline}
              </div>
            </Link>
          ))
          }
        </div>
      </div>
    )
  }
}
export default Search;
