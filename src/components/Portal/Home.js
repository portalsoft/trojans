import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import Search from './Search'

const caller = (bool, func) => {
  if (bool) {
    func();
  }
};

const Home = ({ toggle, toggleSearch, isMenuOpen, recommendedLinkClick, categoriesLinkClick, partnershipsLinkClick, searchClasses, searchCompanies }) => (
  <div className="home">
    <div className="nav nav--filling" />
    <div className="nav">
      <div className="nav__line nav__line--logo">
      </div>
      <div className="nav__line nav__line--logo-text">
        PortalSoft
      </div>
      <a
        href="#recommended"
        className="nav__line"
        onClick={() => {
            recommendedLinkClick();
            toggle();
          }
        }
      >
        <div className='nav__line__text'>
          Most Popular
        </div>
      </a>
      <a
        href="#categories"
        className="nav__line"
        onClick={() => {
            categoriesLinkClick();
            toggle();
          }
        }
      >
        <div className='nav__line__text'>
          Categories
        </div>
      </a>
      <Link
        to="/partnerships"
        className="nav__line"
      >
        <div className='nav__line__text'>
          Partnerships
        </div>
      </Link>
      <Link
        to="about"
        className="nav__line"
      >
        <div className='nav__line__text'>
          About
        </div>
      </Link>
      <Search
        searchClasses={searchClasses}
        toggleSearch={toggleSearch}
        searchCompanies={searchCompanies}
      />
    </div>
    <div className="home__message">
      <div className="home__message__header">
        Startups Rejoice!
      </div>
      <div className="home__message__subheader">
        Never pay full price for business tools again
      </div>
    </div>
    <div className="home__art" />
    <div className="home__description">
      Savings for business, life, and beyond
    </div>
   <div className="home__button-wrapper">
    <Link
      className="home__button"
      to="companies"
    >
      <div className="home__button__button-text">
        Browse All Deals
      </div>
    </Link>
   </div>

  </div>
);
Home.propTypes = {
  isMenuOpen: PropTypes.bool,
  toggle: PropTypes.func,
  toggleSearch: PropTypes.func,
  menuClasses: PropTypes.string,
  menuIconClasses: PropTypes.string,
  recommendedLinkClick: PropTypes.func,
  categoriesLinkClick: PropTypes.func,
  partnershipsLinkClick: PropTypes.func,
  searchClasses: PropTypes.string,
};
export default Home;
