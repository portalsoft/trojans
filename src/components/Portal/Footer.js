import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';

const Footer = ({ openModal, blogClick, contactUsClick, toggleSearch }) => (
  <div className="footer">
    <div className="footer__wrapper">
      <div className="footer__wrapper__category">
        <div className="footer__wrapper__category__title">
          Company
        </div>
        <div className="footer__wrapper__category__text">
          <a
            href="mailto:michael@portalsoft.io?Subject=General%20Inquiry"
            className="footer__wrapper__category__text__section"
            onClick={() => contactUsClick()}
          >
            Contact Us
          </a>
          <Link
            to="faq"
            className="footer__wrapper__category__text__section"
          >
            FAQ
          </Link>
          <Link
            to="about"
            className="footer__wrapper__category__text__section"
          >
            About
          </Link>
          <Link
            to="terms"
            className="footer__wrapper__category__text__section"
          >
            Terms
          </Link>
          <a
            href="https://medium.com/@portalsoft/"
            className="footer__wrapper__category__text__section"
            onClick={() => blogClick()}
          >
            Blog
          </a>
        </div>
      </div>
      <div className="footer__wrapper__category">
        <div className="footer__wrapper__category__title">
          Tools
        </div>
        <div className="footer__wrapper__category__text">
          <a
            href="#recommended"
            className="footer__wrapper__category__text__section"
          >
            Most Popular
          </a>
          <a
            href="#categories"
            className="footer__wrapper__category__text__section"
          >
            Categories
          </a>
          <Link
            to="/partnerships"
            className="footer__wrapper__category__text__section"
          >
            Partnerships
          </Link>
          <div
            className="footer__wrapper__category__text__section"
            onClick={() => toggleSearch()}
          >
            Search
          </div>
          <Link
            to="partner/companies"
            className="footer__wrapper__category__text__section"
          >
            Partner Deals
          </Link>
        </div>
      </div>
      <div className="footer__wrapper__category">
      </div>
      <div className="footer__wrapper__category">
        <div className="footer__wrapper__category__title">
          Connect with Us
        </div>
        <div className="footer__wrapper__category__text">
          <a
            className="icon-container"
            href="https://www.facebook.com/portalsoftco"
          >
            <svg className="icon">
              <use xlinkHref="#icon-Facebook"></use>
            </svg>
          </a>
          <a
            className="icon-container hidden"
            href="https://twitter.com/PortalSoftio"
          >
            <svg className="icon">
              <use xlinkHref="#icon-Twitter"></use>
            </svg>
          </a>
          <a
            className="icon-container"
            href="https://www.linkedin.com/company-beta/18205724/"
          >
            <svg className="icon">
              <use xlinkHref="#icon-LinkedIn"></use>
            </svg>
          </a>
          <div className="footer__wrapper__category__text__section" />
        </div>
      </div>
    </div>
    <div className="footer__spacing" />
  </div>
);
Footer.propTypes = {
  openModal: PropTypes.func,
  toggleSearch: PropTypes.func,
};
export default Footer;
