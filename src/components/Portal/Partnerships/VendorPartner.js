import React from 'react';
import { Link } from 'react-router';

const VendorPartner = () => (
  <div className="vendorpartner">
    <div className="spacer"></div>
    <Link
      to="partner"
      className="partner"
    >
      Communities
    </Link>
    <div className="spacer"></div>
    <Link
      to="vendor"
      className="vendor"
    >
      Vendors
    </Link>
    <div className="spacer"></div>
    <div className="spacer"></div>
    <div className="spacer"></div>
  </div>
);
export default VendorPartner;
