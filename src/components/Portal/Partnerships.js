import React from 'react';
import PropTypes from 'prop-types';

const imageStyles = (backgroundImage) => {
  return {
    backgroundImage: `url(${backgroundImage})`,
    backgroundPosition: 'center',
    backgroundSize: '75% auto',
    backgroundRepeat: 'no-repeat',
    width: '100%',
    flex: '2 1 0%',
    WebkitFlex: '2 1 0%',
  }
}


const Partnerships = ({ workWithTileOne, workWithTileTwo, workWithTileThree, workWithTileFour, workWithTileFive, workWithTileSix }) => (
  <div className="page page--white" id="partnerships">
    <div className="page__header page__header--gray">
      <div className="page__header__text">
        Our Partners
      </div>
    </div>
    <div className="page__tiles">
      <div className="tile tile--category tile--category--flat">
        <div style={imageStyles(workWithTileOne)} />
      </div>
      <div className="tile tile--category tile--category--flat">
        <div style={imageStyles(workWithTileTwo)} />
      </div>
      <div className="tile tile--category tile--category--flat">
        <div style={imageStyles(workWithTileThree)} />
      </div>
      <div className="tile tile--category tile--category--flat">
        <div style={imageStyles(workWithTileFour)} />
      </div>
      <div className="tile tile--category tile--category--flat">
        <div style={imageStyles(workWithTileFive)} />
      </div>
      <div className="tile tile--category tile--category--flat">
        <div style={imageStyles(workWithTileSix)} />
      </div>
    </div>
  </div>
);
Partnerships.propTypes = {
  workWithTileOne: PropTypes.string,
  workWithTileTwo: PropTypes.string,
  workWithTileThree: PropTypes.string,
  workWithTileFour: PropTypes.string,
  workWithTileFive: PropTypes.string,
  workWithTileSix: PropTypes.string,
};
export default Partnerships;

