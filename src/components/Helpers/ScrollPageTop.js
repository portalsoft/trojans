import React from 'react';
import ReactDOM from 'react-dom';

class ScrollPageTop extends React.Component {
  componentDidMount() {
    ReactDOM.findDOMNode(this).scrollIntoView();
  }
  render() {
    return(
      <div className="scroll-top">
      </div>
    )
  }
}
export default ScrollPageTop;
