import React from 'react';
import { Link } from 'react-router';
import ScrollPageTop from './Helpers/ScrollPageTop';

const Splash = () => (
  <div className="splash">
    <ScrollPageTop />
    <h1 className="seo-header">Partnerships</h1>
      <div className="page page--spash-header" id="top">
        <div className="nav nav--black-filling" />
        <div className="nav nav--white-text">
          <div className="nav__line nav__line--logo">
          </div>
          <Link
            className="nav__line nav__line--logo-text"
            to="/"
          >
            PortalSoft
          </Link>
          <a
            href="#features"
            className="nav__line"
          >
            <div className='nav__line__text'>
              Features
            </div>
          </a>
          <a
            href="#customers"
            className="nav__line"
          >
            <div className='nav__line__text'>
              Customers
            </div>
          </a>
          <a
            href="#pricing"
            className="nav__line"
          >
            <div className='nav__line__text'>
              Pricing
            </div>
          </a>
          <a
            href="#contact"
            className="nav__line"
          >
            <div className='nav__line__text'>
              Contact
            </div>
          </a>
        </div>
        <div className="page__splash-spacer" />
        <div className="page__description">
          Invest in your members' success
        </div>
        <div className="page__description page__description--sub">
          Benefits platform for startup communities
        </div>
        <div className="page__button-wrapper">
          <a
            className="page__button"
            href="#contact"
          >
            <div className="page__button__button-text">
              Book Your Benefits Consultation
            </div>
          </a>
        </div>
      </div>
      <div className="page page--half">
        <div className='page__pitch-icon-container'>
          <div className='page__pitch-icon'/>
        </div>
        <div className='page__pitch-text'>
          Experience how PortalSoft creates happy member and productive workspaces in over 40 communities
        </div>
      </div>
      <div className="page page--half">
        <div className='page__blue-container'>
          <div className='page__blue-container__section'>
            <div className='page__blue-container__section__header'>Attract and Retain</div>
            <div className='page__blue-container__section__text'>Differentiate your offering and reduce member turnover with valuable business and lifestyle perks</div>
          </div>
          <div className='page__blue-container__section'>
            <div className='page__blue-container__section__header'>Untapped Revenue</div>
            <div className='page__blue-container__section__text'>Every time your members save, you earn up to 75% commission reveneue sharing</div>
          </div>
          <div className='page__blue-container__section'>
            <div className='page__blue-container__section__header'>Make it your own</div>
            <div className='page__blue-container__section__text'>Enhance your brand valye with curated offer selection and customized braning for white-label customer</div>
          </div>
          <div className='page__blue-container__section'>
            <div className='page__blue-container__section__header'>Admin Dashboard</div>
            <div className='page__blue-container__section__text'>Track usage, redemption and manage members and offers through your unique analytics platform</div>
          </div>
        </div>
      </div>
      <div className="page page--half" id="features">
        <div className='page__pitch-icon-container'>
          <div className='page__pitch-icon page__pitch-icon--computer'/>
        </div>
        <div className='page__pitch-title'>
          Feature Rich Application
        </div>
        <div className='page__pitch-text'>
          Over $10,000 in business and lifestyle savings, enhanced member analytics and unparalleled customer service in one integrated platform
        </div>
      </div>
      <div className="page page--double">
        <div className="page__feature-block-wrapper">
          <div className='page__feature-block'>
            <div className='page__feature-block__image page__feature-block__image--best'></div>
            <div className='page__feature-block__text'>
              <div className='page__feature-block__text__title'>
                Best in Class Offers
              </div>
              <div className='page__feature-block__text__description'>
                Over 50 offers in business and lifestyle categories
              </div>
            </div>
          </div>
          <div className='page__feature-block'>
            <div className='page__feature-block__image page__feature-block__image--revenue'></div>
            <div className='page__feature-block__text'>
              <div className='page__feature-block__text__title'>
                Revenue Share
              </div>
              <div className='page__feature-block__text__description'>
                You earn up to 75% commission on every purchase
              </div>
            </div>
          </div>
          <div className='page__feature-block'>
            <div className='page__feature-block__image page__feature-block__image--space'></div>
            <div className='page__feature-block__text'>
              <div className='page__feature-block__text__title'>
                Space Owner Discounts
              </div>
              <div className='page__feature-block__text__description'>
                Targeted offers for space operators
              </div>
            </div>
          </div>
          <div className='page__feature-block'>
            <div className='page__feature-block__image page__feature-block__image--full-service'></div>
            <div className='page__feature-block__text'>
              <div className='page__feature-block__text__title'>
                Full Service
              </div>
              <div className='page__feature-block__text__description'>
                Dedicated customer support and account management
              </div>
            </div>
          </div>
          <div className='page__feature-block'>
            <div className='page__feature-block__image page__feature-block__image--customization'></div>
            <div className='page__feature-block__text'>
              <div className='page__feature-block__text__title'>
                Full Customization
              </div>
              <div className='page__feature-block__text__description'>
                Add your own branding, stylization and outside offers
              </div>
            </div>
          </div>
          <div className='page__feature-block'>
            <div className='page__feature-block__image page__feature-block__image--accessible'></div>
            <div className='page__feature-block__text'>
              <div className='page__feature-block__text__title'>
                Accessible on any Device
              </div>
              <div className='page__feature-block__text__description'>
                Access your platform from anywhere
              </div>
            </div>
          </div>
          <div className='page__feature-block'>
            <div className='page__feature-block__image page__feature-block__image--site'></div>
            <div className='page__feature-block__text'>
              <div className='page__feature-block__text__title'>
                Site Integration
              </div>
              <div className='page__feature-block__text__description'>
                Over 50 offers in 8 business and lifestyle categories
              </div>
            </div>
          </div>
          <div className='page__feature-block'>
            <div className='page__feature-block__image page__feature-block__image--analytics'></div>
            <div className='page__feature-block__text'>
              <div className='page__feature-block__text__title'>
                User Analytics
              </div>
              <div className='page__feature-block__text__description'>
                Personal Calq.io account to manage offers and user metrics
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="page page--half" id="customers">
        <div className='page__company-blurb'>
          Join over 40 communities using PortalSoft to power their member benefits
        </div>
        <div className='page__pitch-icon-container--companies'>
          <div className='page__pitch-icon page__pitch-icon--fintech'/>
          <div className='page__pitch-icon page__pitch-icon--sosv'/>
          <div className='page__pitch-icon page__pitch-icon--bespoke'/>
          <div className='page__pitch-icon page__pitch-icon--seedstars'/>
        </div>
      </div>
      <div className="page page--triple" id="pricing">
        <div className='page__blue-container page__blue-container--half-white'>
          <div className='page__contact-header'>Clear and Tranparent Pricing</div>
          <div className="page__pricing-boxes">
            <div className='page__pricing-boxes__box'>
              <div className='page__pricing-boxes__box__header'>
                Standard
              </div>
              <div className='page__pricing-boxes__box__price'>
                <div>$20 <span className='page__pricing-boxes__box__price--small'>/ month per 100 users</span></div>
              </div>
              <div className='page__pricing-boxes__box__bullets'>
                <div className='page__pricing-boxes__box__bullets__bullet'>
                  • Access keys to standard platform
                </div>
                <div className='page__pricing-boxes__box__bullets__bullet'>
                  • 25% commission revenue share
                </div>
                <div className='page__pricing-boxes__box__bullets__bullet'>
                  • 3 month minimum contract
                </div>
                <div className='page__pricing-boxes__box__bullets__bullet'>
                  • Monthly analytics reports
                </div>
                <div className='page__pricing-boxes__box__bullets__bullet'>
                  • Space owner discounts
                </div>
              </div>
            </div>
            <div className='page__pricing-boxes__box'>
              <div className='page__pricing-boxes__box__header'>
                Advanced
              </div>
              <div className='page__pricing-boxes__box__price'>
                <div>$25 <span className='page__pricing-boxes__box__price--small'>/ month per 100 users</span></div>
              </div>
              <div className='page__pricing-boxes__box__bullets'>
                <div className='page__pricing-boxes__box__bullets__bullet'>
                  • Custom branded platform
                </div>
                <div className='page__pricing-boxes__box__bullets__bullet'>
                  • 75% commission reveneue share
                </div>
                <div className='page__pricing-boxes__box__bullets__bullet'>
                  • Individual offer selection
                </div>
                <div className='page__pricing-boxes__box__bullets__bullet'>
                  • Private analytics dashboard
                </div>
                <div className='page__pricing-boxes__box__bullets__bullet'>
                  • Website integration
                </div>
                <div className='page__pricing-boxes__box__bullets__bullet'>
                  • Space owner discounts
                </div>
                <div className='page__pricing-boxes__box__bullets__bullet'>
                  • No contract
                </div>
                <div className='page__pricing-boxes__box__bullets__bullet'>
                  • Enhanced customer service
                </div>
              </div>
            </div>
            <div className='page__pricing-boxes__box page__pricing-boxes__box--half'>
              <div className='page__pricing-boxes__box__header page__pricing-boxes__box__header--enterprise'>
                Enterprise
              </div>
              <div className='page__pricing-boxes__box__price page__pricing-boxes__box__price--enterprise' >
                <span className='page__pricing-boxes__box__price--enterprise'>Over 1000 members?</span>
              </div>
              <div className='page__pricing-boxes__box__bullets page__pricing-boxes__box__bullets--enterprise'>
                <div className='page__pricing-boxes__box__bullets__bullet--enterprise'>
                  Reach out to learn about special pricing discounts for large organizations
                </div>
              </div>
            </div>
            <div className="page__button-wrapper page__button-wrapper--pricing">
              <a
                className="page__button"
                href="#contact"
              >
                <div className="page__button__button-text">
                  Contact Sales
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div className="page page--contact" id="contact">
        <div className='page__contact-header'>Get In Touch</div>
        <form className="page__contact-form" method="POST" action="https://formspree.io/info@portalsoft.io">
            <div className='page__contact-form__line'>
              <div className='page__contact-form__line__label'>
                Name
              </div>
              <input className="page__contact-form__line__input" type="text" name="name" />
            </div>
            <div className='page__contact-form__line'>
              <div className='page__contact-form__line__label'>
                Email
              </div>
              <input className="page__contact-form__line__input" type="text" name="_replyto" />
            </div>
            <div className='page__contact-form__line'>
              <div className='page__contact-form__line__label'>
                Phone Number
              </div>
              <input className="page__contact-form__line__input" type="text" name="phone" />
            </div>
            <div className='page__contact-form__line'>
              <div className='page__contact-form__line__label'>
                Organization
              </div>
              <input className="page__contact-form__line__input" type="text" name="organization" />
            </div>
            <div className='page__contact-form__line'>
              <div className='page__contact-form__line__label'>
                Message
              </div>
              <textarea className="page__contact-form__line__input age__contact-form__line__input--area" type="textarea" name="message" />
            </div>
            <input className="page__contact-form__line__input--hidden" type="text" name="_gotcha"/>
            <input type="submit" value="Send" />
        </form>
      </div>
  </div>
);
export default Splash;
