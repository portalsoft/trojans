import Immutable from 'immutable';

const initialState = Immutable.Map({
  isLoggedIn: false,
  message: 'Enter your community code for discount',
});

const headerReducer = (state = initialState, action) => {
  switch (action.type) {
    case '@@router/LOCATION_CHANGE':
      return state.set('isLoggedIn', action.isLoggedIn)
        .set('message', action.message)
    case 'ATTEMPT_LOGIN':
      return state.set('isLoggedIn', action.isLoggedIn)
        .set('message', action.message)
    default:
      return state;
  }
};

export default headerReducer;
