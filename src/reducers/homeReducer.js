/* global calq */
import Immutable from 'immutable';

const initialState = Immutable.Map({
  isMenuOpen: false,
  isSearchOpen: false,
  popularIsLoaded: false,
  searchCompanies: [],
  menuClasses: 'menu',
  menuIconClasses: 'nav__menu-icon',
  searchClasses: 'search-box',
});

const homeReducer = (state = initialState, action) => {
  if (action.type === '@@router/LOCATION_CHANGE' && !state.get('popularIsLoaded')) {
    return state.set('searchCompanies', action.searchCompanies)
      .set('popularIsLoaded', action.popularIsLoaded)
  } else {
    switch (action.type) {
      case 'TOGGLE_MENU':
        if (state.get('isMenuOpen')) {
          calq.action.track('Close Menu');
          return state.set('isMenuOpen', false)
            .set('menuClasses', 'menu')
            .set('menuIconClasses', 'nav__menu-icon');
        }
        calq.action.track('Open Menu');
        return state.set('isMenuOpen', true)
          .set('menuIconClasses', 'nav__menu-icon nav__menu-icon--active')
          .set('menuClasses', 'menu menu--active')
      case 'TOGGLE_SEARCH':
        if (state.get('isSearchOpen')) {
          calq.action.track('Close Search');
          return state.set('isSearchOpen', false)
            .set('searchClasses', 'search-box')
        }
        calq.action.track('Open Search');
        return state.set('isSearchOpen', true)
          .set('searchClasses', 'search-box search-box--active')
      default:
        return state;
    }
  }
};

export default homeReducer;
