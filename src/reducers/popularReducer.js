import Immutable from 'immutable';

const initialState = Immutable.Map({
  popularIsLoaded: false,
  popularTileOne: {},
  popularTileTwo: {},
  popularTileThree: {},
  popularTileFour: {},
  popularTileFive: {},
  popularTileSix: {},
  workWithTileOne: "",
  workWithTileTwo: "",
  workWithTileThree: "",
  workWithTileFour: "",
  workWithTileFive: "",
  workWithTileSix: "",
});

const popularReducer = (state = initialState, action) => {
  if (action.type === '@@router/LOCATION_CHANGE' && !state.get('popularIsLoaded')) {
    return state.set('popularTileOne', action.popularTileOne)
      .set('popularTileTwo', action.popularTileTwo)
      .set('popularTileThree', action.popularTileThree)
      .set('popularTileFour', action.popularTileFour)
      .set('popularTileFive', action.popularTileFive)
      .set('popularTileSix', action.popularTileSix)
      .set('workWithTileOne', action.workWithTileOne)
      .set('workWithTileTwo', action.workWithTileTwo)
      .set('workWithTileThree', action.workWithTileThree)
      .set('workWithTileFour', action.workWithTileFour)
      .set('workWithTileFive', action.workWithTileFive)
      .set('workWithTileSix', action.workWithTileSix)
      .set('popularIsLoaded', action.popularIsLoaded)
  } else {
    return state;
  }
};

export default popularReducer;
