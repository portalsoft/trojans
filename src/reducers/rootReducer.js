import userReducer from './userReducer';
import buttonReducer from './buttonReducer';
import homeReducer from './homeReducer';
import popularReducer from './popularReducer';
import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

const rootReducer = combineReducers({
  userReducer,
  buttonReducer,
  homeReducer,
  popularReducer,
  routing: routerReducer,
});
export default rootReducer;
