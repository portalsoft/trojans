import Immutable from 'immutable';

const initialState = Immutable.Map({
  modalClasses: 'modal',
  modalOverlayClasses: 'modal-overlay',
  backButtonClasses: 'back-button',
  modalParams: {
    category: {
      header: '',
      content: [],
    },
  },
});

const buttonReducer = (state = initialState, action) => {
  switch (action.type) {
    case '@@router/LOCATION_CHANGE':
      return state.set('modalParams', action.modalParams)
        .set('modalClasses', action.modalState.modalClasses)
        .set('modalOverlayClasses', action.modalState.modalOverlayClasses)
        .set('backButtonClasses', action.modalState.backButtonClasses);
    default:
      return state;
  }
};

export default buttonReducer;
