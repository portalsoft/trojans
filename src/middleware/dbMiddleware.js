/* eslint-disable no-param-reassign, no-unused-vars */
/* global calq firebase */
import RouteCalqTracker from './helpers/RouteCalqTracker';
import ActionCalqTracker from './helpers/ActionCalqTracker';

let data;
let popularIsLoaded = false;
let loggedIn = false;

firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
    loggedIn = true;
  } else {
    loggedIn = false;
  }
});

firebase.database().ref().once('value').then((snapshot) => {
  data = snapshot.val();
})

let middlewareLogic = (store, next, action) => {
  if (action.type === '@@router/LOCATION_CHANGE') {
    if (loggedIn) {
      action.isLoggedIn = true;
      action.message = "";
    } else {
      action.isLoggedIn = false;
      action.message = 'Enter your community code for discount';
    }
    if (!popularIsLoaded) {
      action.popularTileOne = data['Most Popular'][0];
      action.popularTileTwo = data['Most Popular'][1];
      action.popularTileThree = data['Most Popular'][2];
      action.popularTileFour = data['Most Popular'][3];
      action.popularTileFive = data['Most Popular'][4];
      action.popularTileSix = data['Most Popular'][5];
      action.workWithTileOne = data.companiesWeWorkWith[1];
      action.workWithTileTwo = data.companiesWeWorkWith[2];
      action.workWithTileThree = data.companiesWeWorkWith[3];
      action.workWithTileFour = data.companiesWeWorkWith[4];
      action.workWithTileFive = data.companiesWeWorkWith[5];
      action.workWithTileSix = data.companiesWeWorkWith[6];
      action.searchCompanies = data.companies;
      popularIsLoaded = true;
      action.popularIsLoaded = true;
    }
    let path = action.payload.pathname;
    let modalParams = {
      backArrow: 'x',
      backPath: '/',
    };
    let modalState = {
      modalClasses: 'modal',
      modalOverlayClasses: 'modal-overlay',
      backButtonClasses: 'back-button',
    };
    const openModalState = {
      modalClasses: 'modal modal--active',
      modalOverlayClasses: 'modal-overlay modal-overlay--active',
      backButtonClasses: 'back-button back-button--active',
    };
    if (path.match(/partner\/companies/g)) {
      if (data.communityCompanies) {
        modalParams.category = {};
        modalParams.category.content = data.communityCompanies;
        modalParams.category.id = 7;
        modalParams.category.header = "Community Offers";
        modalState = openModalState;
      }
    } else if (path.match(/categories\/\d\/companies\/\d+/g)) {
      const companyPath = path.match(/companies\/\d+/g)[0];
      let categoryPath = path.match(/categories\/\d+/g)[0];
      if (categoryPath === "categories/6") {
        categoryPath = "companies";
      }
      if (data.companies[+companyPath.slice(10)]) {
        let company;
        if (categoryPath === "categories/7") {
          categoryPath = "partner/companies";
          company = data.communityCompanies[+companyPath.slice(10)];
        } else {
          company = data.companies[+companyPath.slice(10)];
        }
        modalParams = {
          imgUrl: ("https://portal-soft.imgix.net/" + company.imgUrl.match(/https:\/\/firebasestorage.googleapis.com\/v0\/b\/portalsoft-fec45.appspot.com\/o\/[^?]+/g)[0].slice(75)),
          subCategory: company.subCategory,
          restrictions: company.restrictions,
          screenshots: company.screenshots.map ((screenshot) => {
           return ("https://portal-soft.imgix.net/" + screenshot.match(/https:\/\/firebasestorage.googleapis.com\/v0\/b\/portalsoft-fec45.appspot.com\/o\/[^?]+/g)[0].slice(75))
          }),
          discount: company.discount,
          tagline: company.tagline,
          infoLink: company.infoLink,
          id: company.id,
          name: company.name,
          url: company.url,
          review: company.review,
          writeUp: company.writeUp,
          promo: company.promo,
          seedPromo: company.seedPromo,
          backArrow: '⤺',
          backPath: `/${categoryPath}`,
        };
        modalState = openModalState;
      }
    } else if (path.match(/companies\/\d+/g)) {
      path = path.match(/companies\/\d+/g)[0];
      if (data.companies[+path.slice(10)]) {
        const company = data.companies[+path.slice(10)];
        modalParams = {
          imgUrl: ("https://portal-soft.imgix.net/" + company.imgUrl.match(/https:\/\/firebasestorage.googleapis.com\/v0\/b\/portalsoft-fec45.appspot.com\/o\/[^?]+/g)[0].slice(75)),
          subCategory: company.subCategory,
          restrictions: company.restrictions,
          screenshots: company.screenshots.map ((screenshot) => {
           return ("https://portal-soft.imgix.net/" + screenshot.match(/https:\/\/firebasestorage.googleapis.com\/v0\/b\/portalsoft-fec45.appspot.com\/o\/[^?]+/g)[0].slice(75))
          }),
          discount: company.discount,
          tagline: company.tagline,
          id: company.id,
          infoLink: company.infoLink,
          name: company.name,
          review: company.review,
          url: company.url,
          writeUp: company.writeUp,
          promo: company.promo,
          seedPromo: company.seedPromo,
          backArrow: 'x',
          backPath: '/',
        };
        modalState = openModalState;
      }
    } else if (path.match(/companies/g)) {
      modalParams.category = {};
      modalParams.category.content = data.companies;
      modalParams.category.header = "All Companies";
      modalParams.category.id = 6
      modalState = openModalState;
    } else if (path.match(/categories\/\d+/g)) {
      path = path.match(/categories\/\d+/g)[0];
      if (data.categories[+path.slice(11)]) {
        modalParams.category = data.categories[+path.slice(11)];
        modalState = openModalState;
      }
    } else if (
      path.match(/terms/g) ||
      path.match(/about/g) ||
      path.match(/faq/g) ||
      path.match(/vendor/g)
    ) {
      modalState = openModalState;
    }
    action.modalParams = modalParams;
    action.modalState = modalState;
    RouteCalqTracker(modalParams, modalState, action);
  } else if (action.type === 'ATTEMPT_LOGIN') {
    if (data.codes.includes(action.code.toLowerCase()) !== false) {
      if (action.code.toLowerCase() === 'seed101') {
        window.localStorage.setItem('seedStar', true);
      }
      firebase.auth().signInAnonymously();
      action.isLoggedIn = true;
      action.message = "";
    } else {
      action.message = "Wrong code, try again!"
      action.isLoggedIn = false;
    }
    ActionCalqTracker(action);
  } else if (
    action.type === 'CONTACT_US_CLICK' ||
    action.type === 'BLOG_CLICK' ||
    action.type === 'RECOMMENDED_LINK_CLICK' ||
    action.type === 'CATEGORIES_LINK_CLICK' ||
    action.type === 'PARTNERSHIPS_LINK_CLICK' ||
    action.type === 'CLAIM' ||
    action.type === 'G_TWO_CROWD_CLICK'
    ) {
    ActionCalqTracker(action);
  }
  next(action);
}

const dbMiddleware = store => next => action => {
  if (action.type === 'TOGGLE_MENU') {
    next(action);
  } else {
    if (data == undefined) {
      firebase.database().ref().once('value').then((snapshot) => {
        data = snapshot.val();
        middlewareLogic(store, next, action);
      });
    } else {
      middlewareLogic(store, next, action);
    }
  }
};
export default dbMiddleware;
