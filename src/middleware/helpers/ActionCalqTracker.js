/* global calq */

const ActionCalqTracker = (action, loggedIn)=> {
  let calqAction = {};
  if (action.type === 'CONTACT_US_CLICK') {
    calqAction.name = 'Contact Us';
    calqAction.props = {};
  } else if (action.type === 'BLOG_CLICK') {
    calqAction.name = 'Blog Click';
    calqAction.props = {};
  } else if (action.type === 'RECOMMENDED_LINK_CLICK') {
    calqAction.name = 'Menu Recommended Click';
    calqAction.props = {};
  } else if (action.type === 'CATEGORIES_LINK_CLICK') {
    calqAction.name = 'Menu Categories Click';
    calqAction.props = {};
  } else if (action.type === 'PARTNERSHIPS_LINK_CLICK') {
    calqAction.name = 'Menu Partnerships Click';
    calqAction.props = {};
  } else if (action.type === 'ATTEMPT_LOGIN') {
    if (action.isLoggedIn) {
      calqAction.name = 'Login Success';
      calqAction.props = {code: action.code.toLowerCase()};
    } else {
      calqAction.name = 'Login Failure';
      calqAction.props = {code: action.code.toLowerCase()};
    }
    calqAction.props = {"Code": action.code};
  } else if (action.type === 'CLAIM') {
    calqAction.name = 'Claim Click';
    calqAction.props = {};
  } else if (action.type === 'G_TWO_CROWD_CLICK') {
    calqAction.name = 'G2CROWD Click';
    calqAction.props = {};
  }
  if (calqAction.name) {
    calq.action.track(calqAction.name, calqAction.props)
  }
}
export default ActionCalqTracker;
