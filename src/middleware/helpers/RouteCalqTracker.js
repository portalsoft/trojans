/* global calq */

const RouteCalqTracker = (modalParams, modalState, action)=> {
  let path = action.payload.pathname;
  let calqAction = {}
  if (path.match(/categories\/\d\/companies\/\d+/g)) {
    calqAction = {name: "Category Company View", props: { "Company": modalParams.name }};
  } else if (path.match(/companies\/\d+/g)) {
    calqAction = {name: "Company View", props: { "Company": modalParams.name }};
  } else if (path.match(/companies/g)) {
    calqAction = {name: "Category View", props: { "Category": "Browse All Companies" }};
  } else if (path.match(/categories\/\d+/g)) {
    calqAction = {name: "Category View", props: { "Category": modalParams.category.header }};
  } else if (path.match(/terms/g)) {
    calqAction = {name: "Terms View", props: {}};
  } else if (path.match(/about/g)) {
    calqAction = {name: "About View", props: {}};
  } else if (path.match(/faq/g)) {
    calqAction = {name: "FAQ View", props: {}};
  } else if (path.match(/vendor/g)) {
    calqAction = {name: "Vendor View", props: {}};
  } else if (path.match(/partner/g)) {
    calqAction = {name: "Partner View", props: {}};
  }
  if (calqAction.name) {
    calq.action.track(calqAction.name, calqAction.props)
  }
}
export default RouteCalqTracker;
