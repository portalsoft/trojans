import React from 'react';
import Root from './components/Root.js';
import { render } from 'react-dom';
import './stylesheets/root.css';

if (process.env.REACT_APP_CALQ_WRITE_KEY) {
  window.analyticsToken = process.env.REACT_APP_CALQ_WRITE_KEY;
}

render(
  <Root />,
  document.getElementById('root')
);
