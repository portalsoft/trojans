export const openModal = () => ({ type: 'OPEN_MODAL' });

export const closeModal = () => ({ type: 'CLOSE_MODAL' });

export const toggleMenu = () => ({ type: 'TOGGLE_MENU' });

export const toggleSearch = () => ({ type: 'TOGGLE_SEARCH' });

export const checkLogin = () => ({ type: 'CHECK_LOGIN' });

export const contactUsClick = () => ({ type: 'CONTACT_US_CLICK' });

export const blogClick = () => ({ type: 'BLOG_CLICK' });

export const claim = () => ({ type: 'CLAIM' });

export const recommendedLinkClick = () => ({ type: 'RECOMMENDED_LINK_CLICK' });

export const categoriesLinkClick = () => ({ type: 'CATEGORIES_LINK_CLICK' });

export const partnershipsLinkClick = () => ({ type: 'PARTNERSHIPS_LINK_CLICK' });

export const attemptLogin = (code) => ({
  type: 'ATTEMPT_LOGIN',
  code: code,
});

export const gTwoCrowdClick = () => ({
  type: 'G_TWO_CROWD_CLICK',
});
